package com.example.wtest.Models;

import java.util.ArrayList;
import java.util.List;

public class TextFieldModel {

    public static final List<TextFieldItem> ITEMS = new ArrayList<TextFieldItem>();
    private static final int COUNT = 50;

    static {
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(TextFieldItem item) {
        ITEMS.add(item);
    }

    private static TextFieldItem createDummyItem(int position) {
        return new TextFieldItem(String.valueOf(position));
    }

    public static class TextFieldItem {
        public final String id;

        TextFieldItem(String id) {
            this.id = id;
        }
    }

}
