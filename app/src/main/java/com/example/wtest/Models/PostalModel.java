package com.example.wtest.Models;

import io.realm.RealmObject;

public class PostalModel extends RealmObject {

    public String desig_postal;
    public String num_cod_postal;
    public String ext_cod_postal;

    public PostalModel(){

    }

    public PostalModel(String num_cod_postal, String ext_cod_postal, String desig_postal) {
        this.desig_postal = desig_postal;
        this.num_cod_postal = num_cod_postal;
        this.ext_cod_postal = ext_cod_postal;
    }

    public String getDesig_postal() {
        return desig_postal;
    }

    public void setDesig_postal(String desig_postal) {
        this.desig_postal = desig_postal;
    }

    public String getNum_cod_postal() {
        return num_cod_postal;
    }

    public void setNum_cod_postal(String num_cod_postal) {
        this.num_cod_postal = num_cod_postal;
    }

    public String getExt_cod_postal() {
        return ext_cod_postal;
    }

    public void setExt_cod_postal(String ext_cod_postal) {
        this.ext_cod_postal = ext_cod_postal;
    }
}
