package com.example.wtest.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PostalDatabase extends SQLiteOpenHelper {
    public class Row extends Object {
        public String desig_postal;
        public String num_cod_postal;
        public String ext_cod_postal;
    }

    private SQLiteDatabase db;
    private static final String DATABASE_NAME = "POSTALDB";
    private static final String DATABASE_TABLE = "CODES";
    private static final int DATABASE_VERSION = 4;

    private static final String DATABASE_CREATE =
            "create table CODES(_id integer primary key autoincrement, "
                    + "desig_postal text not null,"
                    + "num_cod_postal text not null,"
                    + "ext_cod_postal text not null"
                    +") ;";



    public PostalDatabase(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(db);
    }

    public void bulkCreateRow(List<Row> rows) {
        List<ContentValues> listOfValues = new ArrayList();
        for(Row row : rows){
            ContentValues initialValues = new ContentValues();
            initialValues.put("desig_postal", row.desig_postal);
            initialValues.put("num_cod_postal", row.num_cod_postal);
            initialValues.put("ext_cod_postal", row.ext_cod_postal);
            listOfValues.add(initialValues);
        }

        db.beginTransaction();
        for(ContentValues val : listOfValues){
            db.insert(DATABASE_TABLE, null, val);
            //System.out.println("Inserted - " + val.getAsString("desig_postal"));
        }
        db.endTransaction();
    }

    public void createRow(String desig_postal, String num_cod_postal, String ext_cod_postal) {
        ContentValues initialValues = new ContentValues();
        initialValues.put("desig_postal", desig_postal);
        initialValues.put("num_cod_postal", num_cod_postal);
        initialValues.put("ext_cod_postal", ext_cod_postal);
        db.insert(DATABASE_TABLE, null, initialValues);
    }

    public void populateDatabase(File file) {
        CSVReader reader = null;

        try {
            String filename = file.list()[0];
            reader = new CSVReader(
                    new FileReader("/data/user/0/com.example.wtest/files/data.csv"), ',');
            Iterator it = reader.iterator();
            it.next();
            List<List<Row>> allRows = new ArrayList<>();

            while(it.hasNext()){
                List<Row> rows = new ArrayList<>();

                for (int i = 0; i < 37500; i++) {
                    String[] line = (String[]) it.next();
                    if (line == null) { continue;}
                    Row row = new Row();
                    row.desig_postal = line[16];
                    row.num_cod_postal = line[14];
                    row.ext_cod_postal = line[15];
                    rows.add(row);
                }
                allRows.add(rows);
            }

            /*SimpleThreadPool pool = new SimpleThreadPool(allRows);
            pool.runThreads();*/

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Row> fetchAllRows() {
        ArrayList<Row> ret = new ArrayList<>();
        try {
            Cursor c =
                    db.rawQuery("SELECT * FROM " + DATABASE_TABLE, null);
            int numRows = c.getCount();
            c.moveToFirst();
            String[] a = c.getColumnNames();
            for (int i = 0; i < 100; ++i) {
                Row row = new Row();
                row.num_cod_postal  = c.getString(3);
                row.ext_cod_postal  = c.getString(2);
                row.desig_postal    = c.getString(1);
                ret.add(row);
                c.moveToNext();
            }
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }
        return ret;
    }

    public Cursor GetAllRows() {
        try {
            return db.query(DATABASE_TABLE, new String[] {"desig_postal"}, "AGADÃO", null, null, null, null);
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
            return null;
        }
    }

    /*public final List<String[]> readCsv(String data) {
        List<String[]> questionList = new ArrayList<String[]>();
        AssetManager assetManager = context.getAssets();

        try {
            InputStream csvStream = assetManager.open(CSV_PATH);
            InputStreamReader csvStreamReader = new InputStreamReader(csvStream);
            CSVReader csvReader = new CSVReader(csvStreamReader);
            String[] line;

            // throw away the header
            csvReader.readNext();

            while ((line = csvReader.readNext()) != null) {
                questionList.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return questionList;
    }*/
}