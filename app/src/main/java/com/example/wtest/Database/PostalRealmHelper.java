package com.example.wtest.Database;

import com.example.wtest.Models.PostalModel;
import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

public class PostalRealmHelper {

    private RealmRecyclerViewAdapter mAdapter;


    public PostalRealmHelper(RealmRecyclerViewAdapter mAdapter){
        this.mAdapter = mAdapter;
    }

    public void populateDatabase(String path) {
        try {
            String filename = path + "/data.csv";
            CSVReader reader = new CSVReader(new FileReader(filename), ',');
            final Iterator it = reader.iterator();
            createObjects(it);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateDatabase(String output) {
        CSVReader reader = new CSVReader(new StringReader(output), ',');
        final Iterator it = reader.iterator();
        createObjects(it);
    }

    private void createObjects(final Iterator it) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                it.next();
                while (it.hasNext()) {
                    final String[] line = (String[]) it.next();
                    PostalModel row = realm.createObject(PostalModel.class);
                    row.setNum_cod_postal(line[14]);
                    row.setExt_cod_postal(line[15]);
                    row.setDesig_postal(line[16]);
                }
            }
        });
    }

    public void createObject(final String num_cod_postal, final String ext_cod_postal, final String desig_postal){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                PostalModel row = realm.createObject(PostalModel.class);
                row.setNum_cod_postal(num_cod_postal);
                row.setExt_cod_postal(ext_cod_postal);
                row.setDesig_postal(desig_postal);
            }
        });
    }

    public void createObject(final List<PostalModel> list){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        int counter = 0;
        for(PostalModel obj : list){
            counter++;
            PostalModel row = realm.createObject(PostalModel.class);
            row.setNum_cod_postal(obj.num_cod_postal);
            row.setExt_cod_postal(obj.ext_cod_postal);
            row.setDesig_postal(obj.desig_postal);
            System.out.println("Added row [" + counter + "/" + list.size() + "]");
        }

        realm.commitTransaction();
    }

    public RealmResults<PostalModel> initResults(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<PostalModel> results = realm.where(PostalModel.class)
                .beginsWith("desig_postal", "")
                .limit(2500)
                .findAll();

        return results;
    }

    public RealmResults<PostalModel> query(String queryString){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<PostalModel> query = realm.where(PostalModel.class);

        String normalizedQuery = queryString.toUpperCase();

        RealmResults<PostalModel> result =
                query.contains("num_cod_postal", normalizedQuery)
                        .or()
                        .contains("ext_cod_postal", normalizedQuery)
                        .or()
                        .contains("desig_postal", normalizedQuery)
                        .findAllAsync();

        return result;
    }

    public RealmResults<PostalModel> getAllRows(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<PostalModel> results = realm.where(PostalModel.class)
                .alwaysTrue()
                .findAllAsync();
        return results;
    }


    /*public void populateDatabaseThreaded(String path) {
        CSVReader reader;

        try {
            String filename = path + "/data.csv";
            reader = new CSVReader(
                    new FileReader(filename), ',');
            Iterator it = reader.iterator();
            it.next();

            List<List<PostalModel>> allRows = new LinkedList<>();

            while(it.hasNext()){
                List<PostalModel> list = new LinkedList<>();
                for (int i = 0; i < NUMBER_OF_ROWS_PER_THREAD; i++) {
                    String[] line = (String[]) it.next();
                    if (line == null) { continue;}
                    PostalModel row = new PostalModel(line[16], line[14], line[15]);
                    list.add(row);
                }
                allRows.add(list);
            }

            SimpleThreadPool pool = new SimpleThreadPool(allRows);
            pool.runThreads();


        } catch (IOException e) {
            System.err.println("Couldn't find \"data.csv\"");
            e.printStackTrace();
        }
    }*/
}
