package com.example.wtest.Cemetery;

public class PostalTask {

    private Runnable postalRunnable;
    private Thread mCurrentThread;
    PostalManager manager;

    PostalTask() {
        //postalRunnable = new PostalRunnable(this);
        manager = PostalManager.getInstance();
    }

    public void initializePostalTask(PostalManager manager)
    {
        // Sets this object's ThreadPool field to be the input argument
        this.manager = manager;
    }

    public Thread getCurrentThread() {
        synchronized(manager) {
            return mCurrentThread;
        }
    }

    public void setCurrentThread(Thread thread) {
        synchronized(manager) {
            mCurrentThread = thread;
        }
    }
}
