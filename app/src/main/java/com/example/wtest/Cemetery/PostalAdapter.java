package com.example.wtest.Cemetery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wtest.Database.PostalDatabase;
import com.example.wtest.R;

import java.util.ArrayList;

public class PostalAdapter extends RecyclerView.Adapter<PostalAdapter.PostalViewHolder>{
    private ArrayList<PostalDatabase.Row> rows;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class PostalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout textView;
        public PostalViewHolder(LinearLayout v) {
            super(v);
            textView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PostalAdapter(ArrayList<PostalDatabase.Row> rows) {
        this.rows = rows;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PostalViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.postal_text_view, parent, false);

        PostalViewHolder vh = new PostalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PostalViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        TextView tv = holder.textView.findViewById(R.id.postal_text);
        String text = rows.get(position).num_cod_postal +  "-" + rows.get(position).ext_cod_postal +", " + rows.get(position).desig_postal;
        tv.setText(text);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return rows.size();
    }
}