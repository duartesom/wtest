package com.example.wtest.Cemetery;

public class PostalRunnable implements Runnable {

    @Override
    public void run() {
        // Moves the current Thread into the background
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        /*
         * Stores the current Thread in the PhotoTask instance,
         * so that the instance
         * can interrupt the Thread.
         */

        PostalTask task = new PostalTask();
        task.setCurrentThread(Thread.currentThread());
    }
}
