package com.example.wtest.Cemetery;

import com.example.wtest.Models.PostalModel;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleThreadPool {

    private static List<List<PostalModel>> allRows;
    private static SimpleThreadPool sInstance = null;
    private static int NUMBER_OF_CORES =
            Runtime.getRuntime().availableProcessors();

    ExecutorService executor;

    /*static {
        sInstance = new SimpleThreadPool();
    }

    public static SimpleThreadPool getInstance() {
        return sInstance;
    }*/

    public SimpleThreadPool(List<List<PostalModel>> allRows){
        this.allRows = allRows;
        executor = Executors.newFixedThreadPool(NUMBER_OF_CORES);
    }

    public void runThreads(){
        for(List<PostalModel> rows : allRows){
            Runnable worker = new WorkerThread(rows);
            executor.execute(worker);
        }
    }

    public void finish(){
        executor.shutdown();
    }

}
