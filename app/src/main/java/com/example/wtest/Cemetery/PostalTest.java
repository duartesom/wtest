package com.example.wtest.Cemetery;

import com.example.wtest.Database.PostalDatabase;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PostalTest {

    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    public static ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_CORES+1);

    public void createRows(List<PostalDatabase.Row> rows, PostalDatabase db){
        Future futureRows = executor.submit(addRows(rows, db));
    }
    private Callable addRows(List<PostalDatabase.Row> rows, PostalDatabase db){
        for(PostalDatabase.Row row : rows){
            db.bulkCreateRow(rows);
            System.out.println("Created row = " + row.num_cod_postal + "-" + row.ext_cod_postal );
        }
        return null;
    }
}

