package com.example.wtest.Cemetery;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PostalManager {

    private static int NUMBER_OF_CORES =
            Runtime.getRuntime().availableProcessors();

    private static PostalManager sInstance = null;
    private final BlockingQueue<Runnable> decodeWorkQueue;
    private final ThreadPoolExecutor decodeThreadPool;
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;

    static {
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
        sInstance = new PostalManager();
    }

    public static PostalManager getInstance() {
        return sInstance;
    }

    private PostalManager() {
        decodeWorkQueue = new LinkedBlockingQueue<>();
        decodeThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                decodeWorkQueue);
    }
}
