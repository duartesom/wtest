package com.example.wtest.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.wtest.Adapter.PostalAdapterRealm;
import com.example.wtest.AsyncTasks.DownloadFileAsync;
import com.example.wtest.AsyncTasks.DownloadFilePartialAsync;
import com.example.wtest.Database.PostalRealmHelper;
import com.example.wtest.Models.PostalModel;
import com.example.wtest.R;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

public class PostalFragment extends Fragment implements DownloadFileAsync.AsyncResponse, DownloadFilePartialAsync.AsyncResponse {

    private RecyclerView recyclerView;
    private RealmRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private View rootView;
    private RealmResults initResults;
    private EditText searchText;
    private PostalRealmHelper helper;
    private ProgressBar spinner;

    AsyncTask<String, String, String> downloadAsyncTask;

    public static PostalFragment newInstance() {
        PostalFragment fragment = new PostalFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setTitle(R.string.title_postal);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Realm.init(getContext());
        //Realm.deleteRealm(Realm.getDefaultConfiguration());
        helper = new PostalRealmHelper(mAdapter);

        String data_path = "https://raw.githubusercontent.com/centraldedados/codigos_postais/master/data/codigos_postais.csv";
        String file_path = checkIfFileExists("data.csv");
        downloadAsyncTask = new DownloadFilePartialAsync(this).execute(data_path);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.postal_fragment, container, false);
        recyclerView = rootView.findViewById(R.id.postal_recycler_view);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new PostalAdapterRealm(null, true, true);
        recyclerView.setAdapter(mAdapter);
        createInitialResults();

        return rootView;
    }

    public void downloadResponse(String output) {
        updateListWithRows(helper);
        spinner.setVisibility(View.GONE);
    }

    @Override
    public void partialDownloadResponse(String output) {
        writeToFile(output);
        helper.updateDatabase(output);
    }

    private void createInitialResults() {
        RealmResults results = helper.initResults();
        mAdapter.updateData(results);
    }

    private void fillDatabase(String path) {
        PostalRealmHelper helper = new PostalRealmHelper(mAdapter);
        helper.populateDatabase(path);
    }

    private void updateListWithRows(final PostalRealmHelper helper) {
        new Runnable() {
            @Override
            public void run() {
                RealmResults results = helper.getAllRows();
                results.addChangeListener(new RealmChangeListener<RealmResults<PostalModel>>() {
                    @Override
                    public void onChange(RealmResults<PostalModel> results) {
                        mAdapter.updateData(results);
                    }
                });
            }
        };
    }

    private void setRecyclerViewContent(RealmResults results){
        recyclerView    = getView().findViewById(R.id.postal_recycler_view);
        layoutManager   = new LinearLayoutManager(getActivity());
        mAdapter        = new PostalAdapterRealm(results, true, true);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    private void initViews(){
        spinner = getActivity().findViewById(R.id.progressBar1);
        searchText = getActivity().findViewById(R.id.query_input);
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                spinner.setVisibility(View.VISIBLE);
                RealmResults results = helper.query(v.getText().toString());
                hideKeyboardFrom(getContext(), getView());
                recyclerView.scrollToPosition(0);
                searchText.clearFocus();

                results.addChangeListener(new RealmChangeListener<RealmResults<PostalModel>>() {
                    @Override
                    public void onChange(RealmResults<PostalModel> results) {
                        mAdapter.updateData(results);
                        spinner.setVisibility(View.GONE);
                    }
                });
                return true;
            }
        });
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private String checkIfFileExists(String filename){
        String path = getActivity().getFilesDir().getPath();
        File file = new File(path + "/" + filename);
        if(file.exists()){
            return file.getPath();
        }
        else return null;
    }

    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getActivity().openFileOutput("data.csv", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        downloadAsyncTask.cancel(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
}
