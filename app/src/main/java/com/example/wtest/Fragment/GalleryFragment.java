package com.example.wtest.Fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.wtest.Adapter.GalleryAdapter;
import com.example.wtest.AsyncTasks.DownloadImageTask;
import com.example.wtest.AsyncTasks.GetImageLinksTask;
import com.example.wtest.Models.GalleryItemModel;
import com.example.wtest.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GalleryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GalleryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GalleryFragment extends Fragment implements GetImageLinksTask.AsyncResponse, DownloadImageTask.AsyncResponse {

    private GalleryAdapter mAdapter;
    private OnFragmentInteractionListener mListener;
    private View rootView;
    private RecyclerView recyclerView;
    private boolean headerImageSet = false;
    private ActionBar toolbar;

    public GalleryFragment() {
    }

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbar();
        new GetImageLinksTask(this).execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.gallery_fragment, container, false);
        ConstraintLayout topContainer =  getActivity().getWindow().getDecorView().findViewById(R.id.top_container);
        ConstraintSet set = new ConstraintSet();
        set.clone(topContainer);
        set.addToVerticalChain(R.id.imageView, R.id.top_container,R.id.navigation);
        set.applyTo(topContainer);

        recyclerView = rootView.findViewById(R.id.gallery_recycler_view);
        setScrollListener();

        if (recyclerView != null) {
            Context context = recyclerView.getContext();
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            mAdapter = new GalleryAdapter();
            recyclerView.setAdapter(mAdapter);
        }
        return rootView;
    }

    private void setToolbar() {

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        toolbar = activity.getSupportActionBar();
        toolbar.setTitle(R.string.title_gallery);
        Drawable background = ContextCompat.getDrawable(getActivity(), R.color.colorPrimary);
        background.setAlpha(0);
        toolbar.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.color.colorPrimary));
    }

    private void setScrollListener() {
        final int ratio = 3;
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                Drawable background = ContextCompat.getDrawable(getActivity(), R.color.colorPrimary);

                int oldAlpha = background.getAlpha();
                if(dy > 0){
                    if(oldAlpha - dy < 0)
                        background.setAlpha(0);
                    else
                        background.setAlpha(oldAlpha - (dy/ratio));
                }else{
                    if(oldAlpha - dy > 255)
                        background.setAlpha(255);
                    else
                        background.setAlpha(oldAlpha - (dy/ratio));
                }
                toolbar.setBackgroundDrawable(background);
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void downloadImageResponse(Bitmap result) {
        GalleryItemModel item = new GalleryItemModel(result);
        ImageView header = rootView.findViewById(R.id.header_image);

        if(!headerImageSet && header != null){
            header.setImageBitmap(item.img);
            headerImageSet = true;
            return;
        }
        mAdapter.addItem(item);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void downloadImageLinksResponse(List<String> list) {
        for (String link : list) {
            new DownloadImageTask(this).execute(link);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
