package com.example.wtest.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.wtest.Fragment.TextFieldFragment.OnListFragmentInteractionListener;
import com.example.wtest.Models.TextFieldModel.TextFieldItem;
import com.example.wtest.R;

import java.util.List;

public class TextFieldAdapter extends RecyclerView.Adapter<TextFieldAdapter.ViewHolder> {

    private final List<TextFieldItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    public TextFieldAdapter(List<TextFieldItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_field_fragment_view_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.textView.setText(mValues.get(position).id);

        if(position % 3 == 0){
            holder.editTextView.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        if(position % 3 == 1){
            holder.editTextView.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        if(position % 3 == 2){
            holder.editTextView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        }


        holder.editTextView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                holder.editTextView.setText(event.getCharacters());
                return true;
            }
        });

        holder.editTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mListener.onLoseFocusEditTextInteraction();
                }
            }
    });
}

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView textView;
        final EditText editTextView;
        TextFieldItem mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            textView = view.findViewById(R.id.text_field_text_view);
            editTextView = view.findViewById(R.id.text_field_edit_text);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + editTextView.getText() + "'";
        }
    }
}
