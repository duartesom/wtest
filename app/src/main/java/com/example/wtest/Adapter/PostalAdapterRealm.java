package com.example.wtest.Adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wtest.Models.PostalModel;
import com.example.wtest.R;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class PostalAdapterRealm extends RealmRecyclerViewAdapter {

    private OrderedRealmCollection data;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class PostalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout textView;
        public PostalViewHolder(LinearLayout v) {
            super(v);
            textView = v;
        }
    }

    @Override
    public void updateData(@Nullable OrderedRealmCollection newData) {
        super.updateData(newData);
        data = newData;
        notifyDataSetChanged();
    }

    public PostalAdapterRealm(@Nullable OrderedRealmCollection data, boolean autoUpdate, boolean updateOnModification) {
        super(data, autoUpdate, updateOnModification);
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.postal_text_view, viewGroup, false);

        PostalViewHolder vh = new PostalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final PostalModel obj = (PostalModel) getItem(position);
        TextView tv = holder.itemView.findViewById(R.id.postal_text);
        String text = obj.num_cod_postal +  "-" + obj.ext_cod_postal +", " + obj.desig_postal;
        tv.setText(text);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if(data == null)
            return 0;
        return data.size();
    }
}