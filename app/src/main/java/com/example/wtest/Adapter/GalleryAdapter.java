package com.example.wtest.Adapter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.wtest.Models.GalleryItemModel;
import com.example.wtest.R;

import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {
    private List<GalleryItemModel> itemList;
    private static final int LIST_SIZE  = 50;

    public GalleryAdapter() {
        itemList = new ArrayList<>(LIST_SIZE);
    }

    public void addItem(GalleryItemModel item) {
        itemList.add(item);
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.gallery_fragment_view_item, viewGroup, false);
        return new GalleryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int i) {
        Bitmap image = itemList.get(i).img;
        holder.imageView.setImageBitmap(image);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public static class GalleryViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public final ImageView imageView;
        public GalleryViewHolder(View view){
            super(view);
            imageView = view.findViewById(R.id.imageView);
        }
    }
}
