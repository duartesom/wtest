package com.example.wtest.AsyncTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private static final int MEGABYTE = 1000000;
    public AsyncResponse res;

    public DownloadImageTask(AsyncResponse delegate){
        this.res = delegate;
    }

    public interface AsyncResponse {
        void downloadImageResponse(Bitmap result);
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        Bitmap bitmap = null;
        URL url;
        try {
            url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            if(urlConnection.getContentLength() > 100 * MEGABYTE)
                this.cancel(true);

            bitmap = BitmapFactory.decodeStream(url.openStream());

        } catch (Exception e) {
            this.cancel(true);
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        res.downloadImageResponse(result);
    }
}
