package com.example.wtest.AsyncTasks;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class GetImageLinksTask extends AsyncTask<String, Void, List<String>> {

    private static final String LINK = "https://alpha.wallhaven.cc/search?q=&categories=100&purity=100&sorting=random&order=desc";
    private static final int NUM_PAGES = 3;
    private static final String downloadUrl = "https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-";
    private static List<String> imageLinks;

    static {
        imageLinks = new LinkedList<>();
    }

    public AsyncResponse res;

    public GetImageLinksTask(AsyncResponse delegate){
        this.res = delegate;
    }

    public interface AsyncResponse {
        void downloadImageLinksResponse(List<String> list);
    }
    @Override
    protected List<String> doInBackground(String... strings) {
        for (int i = 1; i <= NUM_PAGES; i++) {
            fetchImageLinks(i);
        }
        return imageLinks;
    }

    private void fetchImageLinks(int pageNum){
        Document doc;
        try {
            doc = Jsoup.connect(LINK).data("page", String.valueOf(pageNum)).get();
            Elements imageList = doc.select("ul").eq(3).select("li");

            for (Element element : imageList) {
                Element listItem = element.select("a").first();
                String link = listItem.attr("href");
                String[] array = link.split("/");
                String imageID = array[array.length - 1];
                String downloadLink = downloadUrl + imageID + ".jpg";
                imageLinks.add(downloadLink);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(List<String> links) {
        res.downloadImageLinksResponse(links);
    }

}
