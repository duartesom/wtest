package com.example.wtest.AsyncTasks;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFilePartialAsync extends AsyncTask<String,String,String>  {
    String server_response;

    public AsyncResponse res;
    private final int LINES_PER_UPDATE = 10000;

    public interface AsyncResponse {
        void downloadResponse(String output);
        void partialDownloadResponse(String string);
    }

    public DownloadFilePartialAsync(AsyncResponse delegate){
        this.res = delegate;
    }

    @Override
    protected String doInBackground(String... strings) {
        URL url;
        HttpURLConnection urlConnection;

        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection();

            int responseCode = urlConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                readStream(urlConnection.getInputStream());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        res.downloadResponse(server_response);
    }


    @Override
    protected void onProgressUpdate(String... string) {
        res.partialDownloadResponse(server_response);
    }

    private void readStream(InputStream in) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = reader.readLine();

            do {
                StringBuffer response = new StringBuffer();

                for (int i = 0; i < LINES_PER_UPDATE; i++) {
                    line = reader.readLine();
                    response.append(line).append('\n');
                }
                server_response = response.toString();
                onProgressUpdate(server_response);
            }
            while (line != null);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}