package com.example.wtest.Activity;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.wtest.Fragment.GalleryFragment;
import com.example.wtest.Fragment.PostalFragment;
import com.example.wtest.Fragment.TextFieldFragment;
import com.example.wtest.Fragment.WebsiteFragment;
import com.example.wtest.R;

public class MainActivity extends AppCompatActivity implements TextFieldFragment.OnListFragmentInteractionListener,
        GalleryFragment.OnFragmentInteractionListener {

    private PostalFragment postalFragment;
    private GalleryFragment galleryFragment;
    private TextFieldFragment textFragment;
    private WebsiteFragment websiteFragment;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_postal:
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, postalFragment).commit();
                    return true;
                case R.id.navigation_gallery:
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, galleryFragment).commit();
                    return true;
                case R.id.navigation_text:
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, textFragment).commit();
                    return true;
                case R.id.navigation_website:
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, websiteFragment).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(savedInstanceState != null)
            savedInstanceState.clear();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        postalFragment = PostalFragment.newInstance();
        galleryFragment = GalleryFragment.newInstance();
        textFragment = TextFieldFragment.newInstance(1);
        websiteFragment = WebsiteFragment.newInstance();

        navigation.setSelectedItemId(R.id.navigation_gallery);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onLoseFocusEditTextInteraction() {
        hideKeyboardFrom(this, findViewById(R.id.top_container));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
